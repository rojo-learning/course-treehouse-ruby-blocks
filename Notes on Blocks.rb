
=begin

### Ruby Blocks ###

At its most  basic, a block are statements of code that are grouped together.

Multi-line blocks are usually written using the do-end format.

```ruby
do
  # some code here
  # more code here
end
```

Single line blocks are usually written using curly braces.

```ruby
{ "some code here" }
```

Blocks can be used to build cycles with `loop` and `times`. `loop` repeats the
instructions of a block until a `break` statement is reached. `Integer#times`
iterates a block a number of times equal to the integer that issues it.

```ruby
loop do
  puts 'I will run forever!'
  puts '(or until ctrl+c is pressed)'
  sleep 5
end

3.times do |n|
  puts n
end
```

Blocks of code can be passed to methods, and these methods will execute the
received code if they call the `yield` instruction from within.

```ruby
def gimme_a_block
  puts 'first line inside the method'
  yield
  puts 'the line after the yield'
end

gimme_a_block do
  puts 'the line called by the yield!'
end
```

Blocks can also receive arguments!

```ruby
def get_name
  print 'Enter your name: '
  name = gets.chomp
  yield name
end

get_name do |your_name|
  puts "That is a cool name, #{your_name}!"
end
```

With a special syntax, blocks can be turned into objects called _procs_. Also,
it can be checked if a block has been passed to a method.

```ruby
def get_name(prompt, &block)
  print prompt
  name = gets.chomp
  block.call(name) if block_given?
  name
end

my_name = get_name("Name: ") do |your_name|
  puts "That's a cool name, #{your_name}"
end

puts "my_name: #{my_name}"
```

### Strings & Blocks

With blocks, Strings can be easily iterated:

```ruby
  "Hello!".each_char { |char| print "#{char}-" }
  # => H-e-l-l-o-!-

  haiku = <<-EOF
  A string with three lines
  Is considered multi line
  Ruby code haiku
  EOF

  haiku.each_line { |line| puts "* #{line}" }
  # => * A string with three lines
  # => * Is considered multi line
  # => * Ruby code haiku
```

Also, numbers can iterate blocks a fixed number of times:

```ruby
  5.times { print "Hello" }
  # => HelloHelloHelloHelloHello

  1.upto(5) { |n| print n }
  # => 12345

  5.downto(1) { |n| print n }
  # => 54321
```

### Arrays & Blocks

A lot of arrays methods receive blocks to operate with them fast and easy:

```ruby
  array = [1, 2, 3]

  array.each { |item| print "-#{item}-" }
  # => -1--2--3-

  array.select { |item| item > 2 }
  # => [3]

  array.delete_if { |item| item == 1 }
  # => [2, 3]

  array.reject { |item| item % 3 == 0 }
  # => [2]

  array.count
  # => 2
  array.count { |item| item % 3 == 0 }
  # => 1
```

### Hashes & Blocks

In the same way, hashes (dictionaries) use blocks extensively:

```ruby
  hash = { name: 'David', location: 'Internet' }

  hash.each { |k,v| puts "key: #{k} value: #{v}" }
  # => key: name value: David
  # => key: location value: Internet

  hash.each_key { |k| puts "key: #{k}" }
  # => key: name
  # => key: location

  hash.each_value { |v| puts "value: #{v}" }
  # => value: David
  # => value: Internet

  hash.select { |k,v| key == 'name' }
  # => { name: David }

  hash.keep_if { |k,v| key == 'location' }
  # => { location: 'Internet' }

  hash.reject { |k,v| key == 'location' }
  # => {}
```

=end
