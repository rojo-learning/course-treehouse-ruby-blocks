
# Treehouse - Ruby Blocks #

This repository contains notes and practice examples from the course
**Ruby Blocks**, imparted by Jason Seifer at
[Threehouse][JF].

> In this course, you’ll learn all about blocks in Ruby. Blocks are a piece of
> syntax that you can use in Ruby to accomplish all kinds of amazing programming
> feats. Ruby programmers make constant use of blocks so they are an important
> piece of the language to learn.

## Contents ##

- **Ruby Blocks**: In this stage, you'll learn how blocks work in Ruby. You'll
  learn what blocks are, different ways they can be called, and how to pass
  arguments to a block.
- **Working With Blocks**: It's important to learn how blocks work in the real
  world. In this stage, you'll explore how blocks work in some of Ruby's built
  in types. You'll also learn how to create your own classes that implement
  methods with blocks.
- **Blocks Practice**: It's important to practice what you've learned! In this
  stage, we'll practice using blocks by writing a Monster class that makes heavy
  use of blocks, hashes, and more.


---
This repository contains code examples from Treehouse. These are included under
fair use for showcasing purposes only. Those examples may have been modified to
fit my particular coding style.

[JF]: http://teamtreehouse.com
