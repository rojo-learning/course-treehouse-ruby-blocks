
class MyArray
  def initialize
    @array = []
  end

  def push(item)
    @array.push item
  end

  def each(&block)
    @array.size.times { |n| block.call(@array[n]) }
  end
end

my_array = MyArray.new
my_array.push 1
my_array.push 3
my_array.push 5

my_array.each { |item| puts "item: #{item}" }
