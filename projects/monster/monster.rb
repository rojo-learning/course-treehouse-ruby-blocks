
class Monster
  attr_reader :name, :actions

  def initialize(name)
    @name    = name
    @actions = {
      screams: 0,
      scares: 0,
      hides: 0,
      runs: 0
    }
  end

  def say(&block)
    print "#{@name} says... "
    yield if block_given?
    inspect
  end

  def scream(&block)
    actions[:screams] += 1
    print "#{@name} screams! "
    yield if block_given?
    inspect
  end

  def scare(&block)
    actions[:scares] += 1
    print "#{@name} scares you! "
    yield if block_given?
    inspect
  end

  def hide(&block)
    actions[:hides] += 1
    print "#{@name} hides! "
    yield if block_given?
    inspect
  end

  def run(&block)
    actions[:runs] += 1
    print "#{@name} runs! "
    yield if block_given?
    inspect
  end

  def inspect
    puts "#{@name}, #{@actions}"
  end

  def print_scoreboard
    line = "-" * 20

    puts line
    puts "#{@name} scoreboard"
    puts line
    puts "- Screams: #{@actions[:screams]}"
    puts "- Scares: #{@actions[:scares]}"
    puts "- Hides: #{@actions[:hides]}"
    puts "- Runs: #{@actions[:runs]}"
    puts line
  end
end

monster = Monster.new('Fuzzy')
monster.say { puts 'Welcome to my home.' }
monster.scare { puts 'Go away!' }
monster.hide { puts 'Running away and hiding!' }
monster.run { puts 'Going to get you!' }

monster.print_scoreboard
