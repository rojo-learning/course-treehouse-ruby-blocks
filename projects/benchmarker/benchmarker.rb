
class Benchmarker
  def self.time(description)
    puts "» » » » Timing code: #{description}"
    ini_time = Time.now
    yield
    end_time = Time.now
    puts "« « « « Finished execution: #{end_time - ini_time} seconds"
  end
end

Benchmarker.time('count to 100,000') {
  100_000.times {}
}
